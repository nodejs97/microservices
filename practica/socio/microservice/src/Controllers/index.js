const { Model } = require("../Models");

async function Create({ name, phone }) {
    try {
        const instance = await Model.create(
            { name, phone },
            { logging: false }
        );

        return { statusCode: 200, data: instance.toJSON() };
    } catch (error) {
        console.log({ step: "controller Create", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Delete({ where = {} }) {
    try {
        await Model.destroy({ where, logging: false });

        return { statusCode: 200, data: "OK" };
    } catch (error) {
        console.log({ step: "controller Delete", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Update({ id, name, age, phone, email }) {
    try {
        const instance = await Model.update(
            {
                name,
                age,
                phone,
                email,
            },
            {
                where: { id },
                logging: false,
                returning: true, // Solo funciona en Postgres
            }
        );

        return { statusCode: 200, data: instance[1][0].toJSON() };
    } catch (error) {
        console.log({ step: "controller Update", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function FindOne({ where = {} }) {
    try {
        const instance = await Model.findOne({ where, logging: false });

        if (!instance) return { statusCode: 400, message: error.toString() };

        return { statusCode: 200, data: instance.toJSON() };
    } catch (error) {
        console.log({ step: "controller FindOne", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function View({ where = {} }) {
    try {
        const instances = await Model.findAll({ where, logging: false });

        return { statusCode: 200, data: instances };
    } catch (error) {
        console.log({ step: "controller View", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Enable({ id }) {
    try {
        const instance = await Model.update(
            { enable: true },
            {
                where: { id },
                logging: false,
                returning: true, // Solo funciona en Postgres
            }
        );

        return { statusCode: 200, data: instance[1][0].toJSON() };
    } catch (error) {
        console.log({ step: "controller Enable", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Disable({ id }) {
    try {
        const instance = await Model.update(
            { enable: false },
            {
                where: { id },
                logging: false,
                returning: true, // Solo funciona en Postgres
            }
        );

        return { statusCode: 200, data: instance[1][0].toJSON() };
    } catch (error) {
        console.log({ step: "controller Disable", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
    Enable,
    Disable,
};
