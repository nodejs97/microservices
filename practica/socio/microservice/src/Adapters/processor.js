const Services = require("../Services");
const { InternalError } = require("../settings");
const {
    queueCreate,
    queueDelete,
    queueUpdate,
    queueFindOne,
    queueView,
    queueEnable,
    queueDisable,
} = require("./index");

async function Create(job, done) {
    const { name, phone } = job.data;

    try {
        const { statusCode, data, message } = await Services.Create({
            name,
            phone,
        });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueCreate", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function Delete(job, done) {
    const { id } = job.data;

    try {
        const { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueDelete", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function Update(job, done) {
    const { id, name, phone, email } = job.data;

    try {
        const { statusCode, data, message } = await Services.Update({
            id,
            name,
            phone,
            email,
        });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueUpdate", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function FindOne(job, done) {
    const { id } = job.data;

    try {
        const { statusCode, data, message } = await Services.FindOne({ id });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueFindOne", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function View(job, done) {
    try {
        const { enable } = job.data;

        const { statusCode, data, message } = await Services.View({ enable });

        done(null, { statusCode, data: data, message });
    } catch (error) {
        console.log({ step: "adapters queueView", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function Enable(job, done) {
    try {
        const { id } = job.data;

        const { statusCode, data, message } = await Services.Enable({ id });

        done(null, { statusCode, data: data, message });
    } catch (error) {
        console.log({ step: "adapters queueEnable", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function Disable(job, done) {
    try {
        const { id } = job.data;

        const { statusCode, data, message } = await Services.Disable({ id });

        done(null, { statusCode, data: data, message });
    } catch (error) {
        console.log({ step: "adapters queueDisable", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function run() {
    try {
        console.log("Vamos a iniciar worker");

        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueUpdate.process(Update);
        queueFindOne.process(FindOne);
        queueView.process(View);
        queueEnable.process(Enable);
        queueDisable.process(Disable);
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
    Enable,
    Disable,
    run,
};
