const Controllers = require("../Controllers");
const { InternalError } = require("../settings");

async function Create({ name, phone }) {
    try {
        const { statusCode, data, message } = await Controllers.Create({
            name,
            phone,
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services Create", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Delete({ id }) {
    try {
        const findOne = await Controllers.FindOne({ where: { id } });

        if (findOne.statusCode !== 200) {
            switch (findOne.statusCode) {
                case 400:
                    return {
                        statusCode: 400,
                        message: "No existe el socio a eliminar",
                    };

                default:
                    return { statusCode: 500, message: InternalError };
            }
        }

        const { statusCode, data, message } = await Controllers.Delete({
            where: { id },
        });

        if (statusCode === 200) return { statusCode: 200, data: findOne.data };

        return { statusCode: 400, message: InternalError };
    } catch (error) {
        console.log({ step: "services Delete", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Update({ id, name, phone, email }) {
    try {
        const { statusCode, data, message } = await Controllers.Update({
            id,
            name,
            age,
            phone,
            email,
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services Update", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function FindOne({ id }) {
    try {
        const { statusCode, data, message } = await Controllers.FindOne({
            where: { id },
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services FindOne", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function View({ enable }) {
    try {
        const { statusCode, data, message } = await Controllers.View({
            enable,
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services View", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Enable({ id }) {
    try {
        const { statusCode, data, message } = await Controllers.Enable({ id });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services Enable", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Disable({ id }) {
    try {
        const { statusCode, data, message } = await Controllers.Disable({ id });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services Disable", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
    Enable,
    Disable,
};
