const { sequelize } = require("../settings");
const { DataTypes } = require("sequelize");

const Model = sequelize.define("libros", {
    title: { type: DataTypes.STRING },

    category: { type: DataTypes.STRING },

    seccions: { type: DataTypes.STRING },

    image: { type: DataTypes.STRING },
});

const SyncDB = async () => {
    try {
        await Model.sync({ logging: false, force: true });

        return { statusCode: 200, data: "OK" };
    } catch (error) {
        console.log(error);

        return { statusCode: 500, message: error.toString() };
    }
};

module.exports = { SyncDB, Model };
