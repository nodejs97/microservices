const Controllers = require("../Controllers");
const { InternalError } = require("../settings");

async function Create({ title, image }) {
    try {
        const { statusCode, data, message } = await Controllers.Create({
            title,
            image,
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services Create", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Delete({ id }) {
    try {
        const findOne = await Controllers.FindOne({ where: { id } });

        if (findOne.statusCode !== 200) {
            switch (findOne.statusCode) {
                case 400:
                    return {
                        statusCode: 400,
                        message: "No existe el libro a eliminar",
                    };
                default:
                    return { statusCode: 500, message: InternalError };
            }
        }

        const { statusCode, data, message } = await Controllers.Delete({
            where: { id },
        });

        if (statusCode === 200) return { statusCode: 200, data: findOne.data };

        return { statusCode: 400, message: InternalError };
    } catch (error) {
        console.log({ step: "services Delete", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Update({ id, title, image, category, seccions }) {
    try {
        const { statusCode, data, message } = await Controllers.Update({
            id,
            title,
            image,
            category,
            seccions,
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services Update", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function FindOne({ title }) {
    try {
        const { statusCode, data, message } = await Controllers.FindOne({
            where: { title },
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services FindOne", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function View({}) {
    try {
        const { statusCode, data, message } = await Controllers.View({});

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services View", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
};
