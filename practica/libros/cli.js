const api = require("./api");

async function main() {
    try {
        let { data, statusCode, message } = await api.Create({
            image: "https://images.cdn3.buscalibre.com/fit-in/360x360/10/fb/10fb170d7732b7dca25ebb81ded2572d.jpg",
            title: "Clean Code",
        });

        console.log({ data, statusCode, message });
    } catch (err) {
        console.error(err);
    }
}

main();
