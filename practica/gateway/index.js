const express = require("express");
const http = require("http");
const { Server } = require("socket.io");

const app = express();
const server = http.createServer(app);
const io = new Server(server);

const apiLibros = require("api-libros");
const apiSocio = require("api-socio");
const apiPagos = require("api-pagos");

server.listen(3001, () => {
    console.log("Server initialize");

    io.on("connection", (socket) => {
        console.log("New connection", socket.id);

        // Libros
        socket.on("req:libros:view", async ({}) => {
            try {
                console.log("req:libros:view");

                const { statusCode, data, message } = await apiLibros.View({});

                return io.to(socket.id).emit("res:libros:view", {
                    statusCode,
                    data,
                    message,
                });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:libros:create", async ({ title, image }) => {
            try {
                console.log("req:libros:create", { title, image });

                const { statusCode, data, message } = await apiLibros.Create({
                    title,
                    image,
                });

                io.to(socket.id).emit("res:libros:create", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiLibros.View({});

                return io.to(socket.id).emit("res:libros:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        socket.on(
            "req:libros:update",
            async ({ id, title, image, category, seccions }) => {
                try {
                    console.log("req:libros:update", {
                        id,
                        title,
                        category,
                        seccions,
                    });

                    const { statusCode, data, message } =
                        await apiLibros.Update({
                            id,
                            title,
                            category,
                            seccions,
                        });

                    return io.to(socket.id).emit("res:libros:update", {
                        statusCode,
                        data,
                        message,
                    });
                } catch (error) {
                    console.log(error);
                }
            }
        );

        socket.on("req:libros:delete", async ({ id }) => {
            try {
                console.log("req:libros:delete", { id });

                const { statusCode, data, message } = await apiLibros.Delete({
                    id,
                });

                io.to(socket.id).emit("res:libros:delete", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiLibros.View({});

                return io.to(socket.id).emit("res:libros:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:libros:findOne", async ({ title }) => {
            try {
                console.log("req:libros:findOne", { title });

                const { statusCode, data, message } = await apiLibros.FindOne({
                    title,
                });

                return io.to(socket.id).emit("res:libros:findOne", {
                    statusCode,
                    data,
                    message,
                });
            } catch (error) {
                console.log(error);
            }
        });

        // Socio
        socket.on("req:socio:view", async ({ enable }) => {
            try {
                console.log("req:socio:view");

                const { statusCode, data, message } = await apiSocio.View({
                    enable,
                });

                return io.to(socket.id).emit("res:socio:view", {
                    statusCode,
                    data,
                    message,
                });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:socio:create", async ({ name, phone }) => {
            try {
                console.log("req:socio:create", { name, phone });

                const { statusCode, data, message } = await apiSocio.Create({
                    name,
                    phone,
                });

                io.to(socket.id).emit("res:socio:create", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiSocio.View({});

                return io.to(socket.id).emit("res:socio:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:socio:update", async ({ id }) => {
            try {
                console.log("req:socio:update", {
                    id,
                    name,
                    phone,
                    email,
                });

                const { statusCode, data, message } = await apiSocio.Update({
                    id,
                    name,
                    phone,
                    email,
                });

                return io.to(socket.id).emit("res:socio:update", {
                    statusCode,
                    data,
                    message,
                });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:socio:delete", async ({ id }) => {
            try {
                console.log("req:socio:delete", { id });

                const { statusCode, data, message } = await apiSocio.Delete({
                    id,
                });

                io.to(socket.id).emit("res:socio:delete", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiSocio.View({});

                return io.to(socket.id).emit("res:socio:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:socio:findOne", async ({ id }) => {
            try {
                console.log("req:socio:findOne", { id });

                const { statusCode, data, message } = await apiSocio.FindOne({
                    id,
                });

                return io.to(socket.id).emit("res:socio:findOne", {
                    statusCode,
                    data,
                    message,
                });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:socio:enable", async ({ id }) => {
            try {
                console.log("req:socio:enable", { id });

                const { statusCode, data, message } = await apiSocio.Enable({
                    id,
                });

                io.to(socket.id).emit("res:socio:enable", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiSocio.View({});

                return io.to(socket.id).emit("res:socio:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:socio:disable", async ({ id }) => {
            try {
                console.log("req:socio:disable", { id });

                const { statusCode, data, message } = await apiSocio.Disable({
                    id,
                });

                io.to(socket.id).emit("res:socio:disable", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiSocio.View({});

                return io.to(socket.id).emit("res:socio:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        // Pagos
        socket.on("req:pagos:view", async ({}) => {
            try {
                console.log("req:pagos:view");

                const { statusCode, data, message } = await apiPagos.View({});

                return io.to(socket.id).emit("res:pagos:view", {
                    statusCode,
                    data,
                    message,
                });
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:pagos:create", async ({ socio, amount }) => {
            try {
                console.log("req:pagos:create", { socio, amount });

                const { statusCode, data, message } = await apiPagos.Create({
                    socio,
                    amount,
                });

                io.to(socket.id).emit("res:pagos:create", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiPagos.View({});

                return io.to(socket.id).emit("res:pagos:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:pagos:delete", async ({ id }) => {
            try {
                console.log("req:pagos:delete", { id });

                const { statusCode, data, message } = await apiPagos.Delete({
                    id,
                });

                io.to(socket.id).emit("res:pagos:delete", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiPagos.View({});

                return io.to(socket.id).emit("res:pagos:view", view);
            } catch (error) {
                console.log(error);
            }
        });

        socket.on("req:pagos:findOne", async ({ id }) => {
            try {
                console.log("req:pagos:findOne", { id });

                const { statusCode, data, message } = await apiPagos.FindOne({
                    id,
                });

                io.to(socket.id).emit("res:pagos:findOne", {
                    statusCode,
                    data,
                    message,
                });

                const view = await apiPagos.View({});

                return io.to(socket.id).emit("res:pagos:view", view);
            } catch (error) {
                console.log(error);
            }
        });
    });
});
