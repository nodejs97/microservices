const Services = require("../Services");
const { InternalError } = require("../settings");
const {
    queueCreate,
    queueDelete,
    queueFindOne,
    queueView,
} = require("./index");

async function Create(job, done) {
    const { socio, amount } = job.data;

    try {
        const { statusCode, data, message } = await Services.Create({
            socio,
            amount,
        });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueCreate", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function Delete(job, done) {
    const { id } = job.data;

    try {
        const { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueDelete", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function FindOne(job, done) {
    const { id } = job.data;

    try {
        const { statusCode, data, message } = await Services.FindOne({ id });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueFindOne", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function View(job, done) {
    try {
        console.log(job.id);

        const { statusCode, data, message } = await Services.View({});

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: "adapters queueView", error: error.toString() });

        done(null, { statusCode: 500, message: InternalError });
    }
}

async function run() {
    try {
        console.log("Vamos a iniciar worker");

        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueFindOne.process(FindOne);
        queueView.process(View);
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    Create,
    Delete,
    FindOne,
    View,
    run,
};
