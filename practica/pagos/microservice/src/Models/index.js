const { sequelize } = require("../settings");
const { DataTypes } = require("sequelize");

const Model = sequelize.define("pagos", {
    socio: { type: DataTypes.BIGINT },
    amount: { type: DataTypes.BIGINT },
});

const SyncDB = async () => {
    try {
        await Model.sync({ logging: false, force: true });

        return { statusCode: 200, data: "OK" };
    } catch (error) {
        console.log(error);

        return { statusCode: 500, message: error.toString() };
    }
};

module.exports = { SyncDB, Model };
