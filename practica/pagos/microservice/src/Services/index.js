const Controllers = require("../Controllers");
const { InternalError } = require("../settings");
const socios = require("api-socio");

async function Create({ socio, amount }) {
    try {
        const validadSocio = await socios.FindOne({ id: socio });

        if (validadSocio.statusCode !== 200) {
            switch (validadSocio.statusCode) {
                case 400: {
                    return { statusCode: 400, message: "No existe el socio" };
                }
                default: {
                    return { statusCode: 500, message: InternalError };
                }
            }
        }

        if (!validadSocio.data.enable)
            return { statusCode: 400, message: "El socio no está habilitado" };

        const { statusCode, data, message } = await Controllers.Create({
            socio,
            amount,
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services Create", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function Delete({ id }) {
    try {
        const findOne = await Controllers.FindOne({ where: { id } });

        if (findOne.statusCode !== 200) {
            switch (findOne.statusCode) {
                case 400:
                    return {
                        statusCode: 400,
                        message: "No existe el pago a eliminar",
                    };
                default:
                    return { statusCode: 500, message: InternalError };
            }
        }

        const { statusCode, data, message } = await Controllers.Delete({
            where: { id },
        });

        if (statusCode === 200) return { statusCode: 200, data: findOne.data };

        return { statusCode: 400, message: InternalError };
    } catch (error) {
        console.log({ step: "services Delete", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function FindOne({ id }) {
    try {
        const { statusCode, data, message } = await Controllers.FindOne({
            where: { id },
        });

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services FindOne", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

async function View({}) {
    try {
        const { statusCode, data, message } = await Controllers.View({});

        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: "services View", error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = {
    Create,
    Delete,
    FindOne,
    View,
};
