import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { socket } from "./ws";

const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 300px;
    min-width: 300px;
    margin-top: 1rem;
`;

const ContainerBody = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 350px;
    overflow: scroll;
    overflow-x: hidden;
`;

const Socio = styled.div`
    display: flex;
    flex-direction: column;
`;

const Body = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

const Button = styled.button`
    background-color: #f44336;
    color: white;
    padding: 10px 20px;
    opacity: 0.8;
    border-radius: 15px;
    width: 100%;
    height: 50px;
    margin-bottom: 25px;
`;

const Icon = styled.img`
    margin-left: 10px;
    width: 35px;
    height: 35px;
    cursor: pointer;
`;

const Input = styled.input`
    margin-bottom: 10px;
    width: 90%;
    height: 25px;
    border-radius: 2px;
    cursor: pointer;
`;

const Name = styled.p``;

const App = () => {
    const [data, setData] = useState([]);
    const [value, setValue] = useState("");

    const handleCreate = () => {
        socket.emit("req:pagos:create", {
            socio: value,
            amount: 300,
        });
    };

    const handleDelete = (id) => socket.emit("req:pago:delete", { id });

    const handleInput = (e) => {
        setValue(e.target.value);
    };

    useEffect(() => {
        socket.on("res:pagos:view", ({ statusCode, data, message }) => {
            console.log("res:pagos:view", { statusCode, data, message });

            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit("req:pagos:view", {}), 1000);
    }, []);

    return (
        <Container>
            <Input type="number" onChange={handleInput} value={value} />
            <Button onClick={handleCreate}>Aplicar pago para el socio</Button>
            <ContainerBody>
                {data.map((v, i) => (
                    <Socio key={i}>
                        <Body>
                            <Name>Cupón de Pago {v.id}</Name>
                            <Name>Socio: {v.socio}</Name>
                        </Body>
                        <Body>
                            <Name>{v.createdAt}</Name>
                            <Icon
                                src="https://cdn.icon-icons.com/icons2/1154/PNG/512/1486564399-close_81512.png"
                                onClick={() => handleDelete(v.id)}
                            />
                        </Body>
                    </Socio>
                ))}
            </ContainerBody>
        </Container>
    );
};

export default App;
