import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { socket } from "./ws";

const Container = styled.div`
    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: stretch;
    display: flex;
    width: 100%;
`;

const Libro = styled.div`
    flex-direction: column;
    background-color: #303536;
    margin-bottom: 15px;
    border-radius: 10px;
    padding: 2px;
    display: flex;
    position: relative;
`;

const Portada = styled.img`
    width: 200px;
`;

const Title = styled.p`
    color: white;
    text-align: center;
`;

const Icon = styled.img`
    width: 35px;
    height: 35px;
    position: absolute;
    top: -6px;
    right: -5px;
    cursor: pointer;
`;

const Button = styled.button`
    background-color: #036e00;
    color: white;
    padding: 10px 20px;
    opacity: 0.8;
    border-radius: 15px;
    width: 100%;
    height: 50px;
    margin-bottom: 25px;
`;

const Item = ({ image, title, id }) => {
    const handleDelete = () => socket.emit("req:libros:delete", { id });

    const opts = {
        src: "https://cdn.icon-icons.com/icons2/1154/PNG/512/1486564399-close_81512.png",
        onClick: handleDelete,
    };

    return (
        <Libro>
            <Icon {...opts} />
            <Portada src={image} />
            <Title>{title}</Title>
        </Libro>
    );
};

const App = () => {
    const [data, setData] = useState([]);

    const handleCreate = () => {
        socket.emit("req:libros:create", {
            image: "https://images.cdn3.buscalibre.com/fit-in/360x360/10/fb/10fb170d7732b7dca25ebb81ded2572d.jpg",
            title: "Clean Code",
        });
    };

    useEffect(() => {
        socket.on("res:libros:view", ({ statusCode, data, message }) => {
            console.log("res:libros:view", { statusCode, data, message });

            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit("req:libros:view", {}), 1000);
    }, []);

    return (
        <Container>
            <Button onClick={handleCreate}>Create</Button>

            {data.map((v, i) => (
                <Item key={i} {...v} />
            ))}
        </Container>
    );
};

export default App;
