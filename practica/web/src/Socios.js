import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { socket } from "./ws";

const Container = styled.div`
    width: 300px;
    min-width: 300px;
`;

const ContainerBody = styled.div`
    height: 350px;
    overflow-y: scroll;
    overflow-x: hidden;
`;

const Socio = styled.div`
    display: flex;
    flex-direction: column;
`;

const Body = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

const Name = styled.p``;

const Phone = styled.p``;

const Email = styled.p``;

const Enable = styled.p`
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background-color: ${(props) => (props.enable ? "green" : "red")};
    cursor: pointer;
`;

const Button = styled.button`
    background-color: #f44336;
    color: white;
    padding: 10px 20px;
    opacity: 0.8;
    border-radius: 15px;
    width: 100%;
    height: 50px;
    margin-bottom: 25px;
`;

const Icon = styled.img`
    margin-left: 10px;
    width: 35px;
    height: 35px;
    cursor: pointer;
`;

const FeedBack = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const App = () => {
    const [data, setData] = useState([]);

    const handleCreate = () => {
        socket.emit("req:socio:create", {
            name: "Harold Navarro",
            phone: new Date().getTime(),
        });
    };

    const handleDelete = (id) => socket.emit("req:socio:delete", { id });

    const handleChange = (id, status) => {
        if (status) return socket.emit("req:socio:disable", { id });

        return socket.emit("req:socio:enable", { id });
    };

    useEffect(() => {
        socket.on("res:socio:view", ({ statusCode, data, message }) => {
            console.log("res:socio:view", { statusCode, data, message });

            if (statusCode === 200) setData(data.sort((a, b) => a.id - b.id));
        });

        setTimeout(() => socket.emit("req:socio:view", {}), 1000);
    }, []);

    return (
        <Container>
            <Button onClick={handleCreate}>Create</Button>
            <ContainerBody>
                {data.map((v, i) => (
                    <Socio key={i}>
                        <Body>
                            <Name>
                                {v.name} <small>{v.id}</small>
                            </Name>
                            <Phone>{v.phone}</Phone>
                        </Body>
                        <Body>
                            <Email>{v.email}</Email>
                            <FeedBack>
                                <Enable
                                    enable={v.enable}
                                    onClick={() => handleChange(v.id, v.enable)}
                                />
                                <Icon
                                    src="https://cdn.icon-icons.com/icons2/1154/PNG/512/1486564399-close_81512.png"
                                    onClick={() => handleDelete(v.id)}
                                />
                            </FeedBack>
                        </Body>
                    </Socio>
                ))}
            </ContainerBody>
        </Container>
    );
};

export default App;
